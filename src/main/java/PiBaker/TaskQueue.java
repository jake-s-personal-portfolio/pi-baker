//package PiBaker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class TaskQueue {
    private LinkedList<Task> queue = new LinkedList<Task>();
    private ArrayList<Task> builder = new ArrayList<Task>();
    
    TaskQueue(){
//        fill arraylist with a 1000 tasks 
        for(int x = 1; x<=1000;x++){
            Task t = new Task(x);
            builder.add(t);
        }
//        randomize the list of tasks
        Collections.shuffle(builder);

//        build the linked list from the arraylist
        for (Task t:builder
             ) {
            queue.add(t);
        }
    }

    public boolean isQueueEmpty(){
        return queue.isEmpty();
    }

    synchronized public Task getTask(){
        return queue.remove();
    }
}
