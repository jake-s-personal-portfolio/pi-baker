//package PiBaker;

public class Employ implements Runnable{

    private TaskQueue queue;
    private ResultTable table;
    private int taskRan = 0;

    Employ(TaskQueue queue, ResultTable table){
        this.queue = queue;
        this.table = table;
    }

    public void run() {
        while (!queue.isQueueEmpty()) {
            try {
                Task job = queue.getTask();
                table.add(job.getIndex(), job.calculate());
                taskRan++;

                if (taskRan % 200 == 0){
                    System.out.println(".");
                    System.out.flush();
                }
                else if (taskRan % 10 == 0) {
                    System.out.print(".");
                    System.out.flush();
                }
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }


}
