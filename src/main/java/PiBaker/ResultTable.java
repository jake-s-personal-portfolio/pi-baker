//package PiBaker;

import java.util.HashMap;

public class ResultTable {
    private HashMap table = new HashMap();

    ResultTable(){
    }

    synchronized public void add(int key, int value){
        table.put(key,value);
    }

    synchronized public int getValue(int key){
        return (int)table.get(key);
    }

    public int getTableSize(){
        return table.size();
    }
}
