//package PiBaker;

// Don't forget to implement runnable
public class Task{
    private Bpp oven = new Bpp();
    private long index;

    Task(long index){
        this.index =index;
    }

    public int calculate(){
        String pieceOfPi = Integer.toString(oven.getDecimal(index));

        if(pieceOfPi.length()==9){
            return Integer.parseInt(pieceOfPi.substring(0,1));
        }
        else{
            return 0;
        }
    }

    public int getIndex() {
        return (int)index;
    }
}
